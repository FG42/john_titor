(ns john-titor.core
  (:require
   [hellhound.system :as sys]
   [hellhound.system.development :as d]
   [john-titor.system :as system])
  (:gen-class))

(d/setup-development john-titor.core  'system/dev)

(defn -main
  [& args]
  (sys/set-system! system/dev)
  (sys/start!))
