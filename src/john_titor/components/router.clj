(ns john-titor.components.router
  (:require
   [hellhound.message :as msg]
   [hellhound.component :refer [deftransform]]))


(defn find-in
  [routes uri]
  (get routes uri :not-found))

(defn find-route
  [request routes]
  (if-not request
    nil
    (find-in (get routes (:request-method request) {})
             (:uri request))))


(deftransform router
  [component v]
  (if (:request v)
    (assoc v :type
             (find-route (:request v)
                         (:routes (:config (:context component)))))
    v))
