(ns john-titor.components.json
  (:require
   [cheshire.core :as j]
   [byte-streams :as bs]
   [hellhound.component :refer [deftransform]]))


(deftransform body-parser
  [this v]
  (if-let [body (:body (or (:request v) {}))]
    ;; TODO: Handle json parse error
    ;; TODO: Body might be really big we need to switch to streams
    (assoc v :payload (j/parse-string (bs/to-string body) true))
    v))
