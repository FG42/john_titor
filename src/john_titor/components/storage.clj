(ns john-titor.components.storage
  (:require
   [john-titor.storage :as impl]
   [hellhound.message :as msg]
   [hellhound.component :as com :refer [deftransform!]]))

(def storage-engine (atom {}))

(defmulti process-command
  (fn [engine message] (:command message)))

(defmethod process-command "read"
  [engine message]
  (impl/<-storage engine
                  (:key message)
                  (:default-value message)))

(defmethod process-command "write"
  [engine message]
  (impl/->storage engine
                  (:key message)
                  (:value message))
  ::ok)

(defmethod process-command :default
  [engine message]
  ::do-nothing)


(deftransform! engine
  [component v]
  (assoc v ::result (process-command storage-  engine (::operation v))))
