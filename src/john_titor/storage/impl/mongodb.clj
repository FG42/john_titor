(ns john-titor.storage.impl.mongodb
  (:require
   [monger.core :as mg]
   [monger.collection :as mc]
   [monger.operators :as op]
   ;; TODO: Remove mcr after testing
   [monger.credentials :as mcr]
   [john-titor.storage.protocols :as protocol]
   [hellhound.component :as com])
  (:import
   [org.bson.types ObjectId]
   [com.mongodb DuplicateKeyException]))


(def keyspaces-coll "keyspaces")

(defn- insert-keyspace
  [db keyspace]
  (try
    (mc/insert db
               keyspaces-coll
               {:_id (ObjectId.)
                :name keyspace
                :created-at nil
                :data {}})
    (catch DuplicateKeyException e
      (throw (ex-info "Keyspace already exists."
                      {:cause keyspace})))))


(defn- create-keyspace
  [db storage keyspace]
  (insert-keyspace db keyspace))

(defn- delete-keyspace
  [db keyspace]
  (mc/remove db keyspaces-coll {:name keyspace}))

(defn- list-keys
  [db keyspace]
  (keys (:data (mc/find-one-as-map db keyspaces-coll {:name keyspace}))))

(defn create-keyspace-index
  [db]
  (mc/ensure-index db
                   keyspaces-coll
                   (array-map :name 1)
                   {:unique true}))

(deftype MongoBaseStorageManager [db storage]
  protocol/KeySpaceManagement
  (create-keyspace
    [this keyspace]
    (create-keyspace db storage keyspace))

  (delete-keyspace
    [this keyspace]
    (delete-keyspace db keyspace))

  (list-keys
    [this keyspace]
    (list-keys db keyspace))

  protocol/WritableStorage
  (->storage
    [this keyspace k v]
    (mc/update db keyspaces-coll {:name keyspace} {op/$set {:data {k v}}})))

(extend-type MongoBaseStorageManager)



(comment
  (def u "root")
  (def p (.toCharArray "example"))
  (def conn (mg/connect-with-credentials "localhost" (mcr/create u "admin" p)))
  (def db (mg/get-db conn "john_titor"))
  (def m (->MongoBaseStorageManager db "s"))
  (create-keyspace-index db)
  (mg/disconnect conn)
  (protocol/create-keyspace m "sameer")
  (protocol/create-keyspace m "mary")
  (protocol/delete-keyspace m "sameer")
  (protocol/list-keys m "mary")
  (protocol/->storage m "mary" :a 3)
  (clojure.pprint/pprint (keys (:data (mc/find-one-as-map db keyspaces-coll {:name "mary"})))))
