(ns john-titor.storage.impl.s3
  "Storage implementation for S3 compatible storages."
  (:require
   [amazonica.aws.s3 :as s3]
   [amazonica.aws.s3transfer :as s3t]
   [john-titor.storage.protocols :as protocol]))


(defn auth-info
  []
  {:access-key "aws-access-key"
   :secret-key "aws-secret-key"
   :endpoint   "ams"})


(deftype S3Storage [config]
  protocol/WritableStorage
  (->storage
    [storage keyspace k v]
    true)

  (delete-key
    [storage keyspace k]
    true)

  protocol/KeySpaceManagement
  (create-keyspace
    [storage keyspace]
    (s3/create-bucket config keyspace))

  (delete-keyspace
    [storage keyspace]
    true))

(extend-type S3Storage
  protocol/ReadableStorage
  (<-storage
    ([storage keyspace k] true)
    ([storage keyspace k default-value] true)))
