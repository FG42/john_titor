(ns john-titor.storage.impl.atom
  (:require
   [john-titor.storage.protocols :as protocols]))


(extend-type clojure.lang.IAtom
  protocols/ReadableStorage
  (<-storage
    ([storage k]
     (get @storage k))
    ([storage k default-value]
     (get @storage k default-value)))

  protocols/WritableStorage
  (->storage
    [storage k v]
    (swap! storage
           assoc
           k
           v)))
