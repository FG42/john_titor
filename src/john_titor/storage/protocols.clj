(ns john-titor.storage.protocols)


(defprotocol ReadableStorage
  (<-storage
    [storage keyspace k]
    [storage keyspace k default-value]
    "Reads the value of the given `k` from the given `storage` and `keyspace`
    and returns the given `default-value` if the actual value was nil."))

(defprotocol WritableStorage
  (->storage
    [storage keyspace k velue]
    "Writes the given `value` to for the given `k` into the given `storage`
    and `keyspace`.")

  (delete-key
    [storage keyspace k]
    "Deletes the given `k` from the given `storage` and `keyspace`."))

(defprotocol KeySpaceManagement
  (create-keyspace
    [storage keyspace]
    "Creates the given `keyspace` in the given `storage`.")

  (delete-keyspace
    [storage keyspace]
    "Removes the given `keyspace` from the given `storage`.")

  (list-keys
    [storage keyspace]
    "Returns a collection of keys inside the given `keyspace`."))
