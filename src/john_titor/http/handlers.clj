(ns john-titor.http.handlers
  (:require
   [hellhound.http.ring.template :as template]
   [hellhound.component :refer [deftransform]]))


(deftransform home-page
  [component value]
  ;;(assoc v :response (template/render "public/index.html" {})))
  (assoc value :response {:status 200 :body "HOME PAGE"}))

(deftransform not-found
  [component value]
  (assoc value :response {:status 404 :body "NOT FOUND!"}))


(deftransform query
  [component value]
  (assoc value :john-titor.components.storage/operation (:payload value)))


(deftransform query-result
  [component value]
  (assoc value
         :response
         {:status 200 :body (str (:john-titor.components.storage/result value))}))
