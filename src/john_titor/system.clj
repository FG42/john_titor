(ns john-titor.system
  (:require
   [hellhound.system :as sys]
   [hellhound.component :as com]
   [hellhound.components.webserver :as webserver]
   [hellhound.components.pretty-printer :as printer]
   [john-titor.components
    [storage :as storage]
    [router  :as router]
    [json    :as json]]
   [john-titor.http.handlers :as handlers]))

(defn is-type?
  [v t]
  (= (:type v) t))


(defn dev
  []
  {:components [(webserver/factory)
                printer/printer
                handlers/not-found
                handlers/home-page
                handlers/query
                handlers/query-result
                router/router
                json/body-parser
                storage/engine]

   :workflow
   [

    {:hellhound.workflow/source ::webserver/webserver
     :hellhound.workflow/sink ::json/body-parser}

    {:hellhound.workflow/source ::json/body-parser
     :hellhound.workflow/sink ::router/router}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/predicate #(is-type? % :not-found)
     :hellhound.workflow/sink ::handlers/not-found}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/predicate #(is-type? % :query)
     :hellhound.workflow/sink ::handlers/query}

    {:hellhound.workflow/source ::handlers/query
     :hellhound.workflow/sink ::storage/engine}

    {:hellhound.workflow/source ::storage/engine
     :hellhound.workflow/sink ::handlers/query-result}

    {:hellhound.workflow/source ::handlers/query-result
     :hellhound.workflow/sink ::webserver/webserver}

    {:hellhound.workflow/source ::handlers/query-result
     :hellhound.workflow/sink ::printer/printer}

    {:hellhound.workflow/source ::handlers/not-found
     :hellhound.workflow/sink ::webserver/webserver}]

   :execution {:mode :multi-thread}
   :logger {:level :debug}
   :config
   {::webserver/webserver {:host "localhost"
                           :port 3000
                           :scheme "http"}
    ::router/router {:routes {:get {"/q" :query
                                    "/"  :home-page}
                              :post {}}}}})



    ;; {:hellhound.workflow/source ::webserver/webserver
    ;;  :hellhound.workflow/sink ::json/body-parser}

    ;; {:hellhound.workflow/source ::json/body-parser
    ;;  :hellhound.workflow/sink ::router/router}

    ;; {:hellhound.workflow/source ::router/router
    ;;  :hellhound.workflow/predicate home-page?
    ;;  :hellhound.workflow/sink ::handlers/home-page}

    ;; {:hellhound.workflow/source ::router/router
    ;;  :hellhound.workflow/predicate not-found?
    ;;  :hellhound.workflow/sink ::handlers/not-found}

    ;; {:hellhound.workflow/source ::router/router
    ;;  :hellhound.workflow/predicate query?
    ;;  :hellhound.workflow/sink ::storage/engine}

    ;; {:hellhound.workflow/source ::router/router
    ;;  :hellhound.workflow/sink ::printer/printer}


    ;; {:hellhound.workflow/source ::handlers/home-page
    ;;  :hellhound.workflow/sink ::webserver/webserver}

    ;; {:hellhound.workflow/source ::handlers/not-found
    ;;  :hellhound.workflow/sink ::webserver/webserver}]
