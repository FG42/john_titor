(defproject john_titor "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure      "1.10.0"]
                 [manifold                 "0.1.9-alpha3"]
                 [codamic/hellhound.core   "1.0.0-SNAPSHOT"]
                 [codamic/hellhound.http   "1.0.0-SNAPSHOT"]
                 [codamic/hellhound.utils  "1.0.0-SNAPSHOT"]
                 [cheshire                 "5.8.1"]
                 [com.novemberain/monger   "3.1.0"]
                 [amazonica "0.3.136" :exclusions [com.amazonaws/aws-java-sdk
                                                   com.amazonaws/amazon-kinesis-client]]
                 [com.amazonaws/aws-java-sdk-core "1.11.391"]
                 [com.amazonaws/aws-java-sdk-s3 "1.11.391"]]



  :main ^:skip-aot john-titor.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
